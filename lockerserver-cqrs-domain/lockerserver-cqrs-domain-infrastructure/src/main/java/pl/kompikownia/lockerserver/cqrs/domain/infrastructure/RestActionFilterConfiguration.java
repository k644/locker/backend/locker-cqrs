package pl.kompikownia.lockerserver.cqrs.domain.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.CommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.QueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.InternalModuleRemoteCommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.InternalModuleRemoteQueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter.ProcessRestActionFilter;

@Configuration
public class RestActionFilterConfiguration {

    @Bean
    public ProcessRestActionFilter processRestActionFilter(
            final InternalModuleRemoteQueryHandlerFactory internalModuleRemoteQueryHandlerFactory,
            final QueryHandlerFactory queryHandlerFactory,
            final InternalModuleRemoteCommandHandlerFactory internalModuleRemoteCommandHandlerFactory,
            final CommandHandlerFactory commandHandlerFactory,
            final ContextHolder<ModuleContext> moduleContextHolder,
            final ContextFactory<ModuleContext> moduleContextFactory
            ) {
        return new ProcessRestActionFilter(
                internalModuleRemoteQueryHandlerFactory,
                internalModuleRemoteCommandHandlerFactory,
                queryHandlerFactory,
                commandHandlerFactory,
                moduleContextHolder,
                moduleContextFactory
        );
    }
}
