package pl.kompikownia.lockerserver.cqrs.domain.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.external.module.ExternalModuleContext;
import pl.kompikownia.lockerserver.common.utils.domain.locker.module.LockerModuleContextDataProvider;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.CommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.ExternalModuleRemoteCommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.ExternalModuleRemoteQueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.QueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter.ProcessExternalRestActionFilter;

@Configuration
public class ExternalRestActionFilterConfiguration {

    @Bean
    public ProcessExternalRestActionFilter processExternalRestActionFilter(
            final ExternalModuleRemoteQueryHandlerFactory externalModuleRemoteQueryHandlerFactory,
            final ExternalModuleRemoteCommandHandlerFactory externalModuleRemoteCommandHandlerFactory,
            final QueryHandlerFactory queryHandlerFactory,
            final CommandHandlerFactory commandHandlerFactory,
            final ContextHolder<ExternalModuleContext> externalModuleContextHolder,
            final ContextFactory<ExternalModuleContext> externalModuleContextFactory,
            final LockerModuleContextDataProvider lockerModuleContextDataProvider
            ) {
        return new ProcessExternalRestActionFilter(
                externalModuleRemoteQueryHandlerFactory,
                externalModuleRemoteCommandHandlerFactory,
                queryHandlerFactory,
                commandHandlerFactory,
                externalModuleContextHolder,
                externalModuleContextFactory,
                lockerModuleContextDataProvider
        );
    }
}
