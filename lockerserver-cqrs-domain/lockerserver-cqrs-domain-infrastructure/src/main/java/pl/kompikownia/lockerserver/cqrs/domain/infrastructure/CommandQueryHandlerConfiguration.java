package pl.kompikownia.lockerserver.cqrs.domain.infrastructure;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.util.ClassUtils;
import org.springframework.web.client.RestTemplate;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.UserContext;
import pl.kompikownia.lockerserver.cqrs.domain.api.annotation.ExternalRestAction;
import pl.kompikownia.lockerserver.cqrs.domain.api.annotation.RestAction;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.Protocol;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceUrlProvider;
import pl.kompikownia.lockerserver.cqrs.domain.application.executor.CommandExecutorImpl;
import pl.kompikownia.lockerserver.cqrs.domain.application.executor.QueryExecutorImpl;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.*;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.adapter.ServiceExecutorImpl;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.adapter.ServiceUrlProviderImpl;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

@Configuration
@Slf4j
public class CommandQueryHandlerConfiguration {

    @Bean
    public QueryHandlerFactory queryHandlerFactory() {
        return new QueryHandlerFactory();
    }

    @Bean
    public CommandHandlerFactory commandHandlerFactory() {
        return new CommandHandlerFactory();
    }

    @Bean
    public QueryExecutor queryExecutor(
            final QueryHandlerFactory queryHandlerFactory,
            final ServiceExecutor serviceExecutor,
            @Value("${pl.kompikownia.lockerserver.cqrs.serviceExecutor.protocol}")
            final Protocol protocol
    ) {
        return new QueryExecutorImpl(queryHandlerFactory, serviceExecutor, protocol);
    }

    @Bean
    public CommandExecutor commandExecutor(
            final CommandHandlerFactory commandHandlerFactory,
            final ServiceExecutor serviceExecutor,
            @Value("${pl.kompikownia.lockerserver.cqrs.serviceExecutor.protocol}")
            final Protocol protocol) {
        return new CommandExecutorImpl(commandHandlerFactory, serviceExecutor, protocol);
    }

    @Bean
    public ServiceUrlProvider serviceUrlProvider(final Environment environment) {
        return new ServiceUrlProviderImpl(environment);
    }

    @Bean
    public ServiceExecutor serviceExecutor(
            final ServiceUrlProvider serviceUrlProvider,
            final ContextHolder<? extends UserContext> contextHolder,
            final RestTemplate serviceRestTemplate,
            final RestTemplate externalRestTemplate) {
        return new ServiceExecutorImpl(serviceUrlProvider, contextHolder, serviceRestTemplate, externalRestTemplate);
    }

    @Bean
    public RestTemplate externalRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public InternalModuleRemoteQueryHandlerFactory remoteQueryHandlerFactory() {
        return new InternalModuleRemoteQueryHandlerFactory();
    }

    @Bean
    public InternalModuleRemoteCommandHandlerFactory remoteCommandHandlerFactory() {
        return new InternalModuleRemoteCommandHandlerFactory();
    }

    @Bean
    public ExternalModuleRemoteQueryHandlerFactory externalModuleRemoteQueryHandlerFactory() {
        return new ExternalModuleRemoteQueryHandlerFactory();
    }

    @Bean
    public ExternalModuleRemoteCommandHandlerFactory externalModuleRemoteCommandHandlerFactory() {
        return new ExternalModuleRemoteCommandHandlerFactory();
    }

    @Configuration
    public static class HandlerFactoriesConfiguration {

        @Autowired(required = false)
        private Collection<QueryHandler> queryHandlers = new ArrayList<>();

        @Autowired(required = false)
        private Collection<CommandHandler> commandHandlers = new ArrayList<>();

        @Autowired
        private CommandHandlerFactory commandHandlerFactory;

        @Autowired
        private QueryHandlerFactory queryHandlerFactory;

        @Autowired
        private InternalModuleRemoteQueryHandlerFactory internalModuleRemoteQueryHandlerFactory;

        @Autowired
        private InternalModuleRemoteCommandHandlerFactory internalModuleRemoteCommandHandlerFactory;

        @Autowired
        private ExternalModuleRemoteQueryHandlerFactory externalModuleRemoteQueryHandlerFactory;

        @Autowired
        private ExternalModuleRemoteCommandHandlerFactory externalModuleRemoteCommandHandlerFactory;

        @PostConstruct
        protected void initializeHandlerFactories() {
            AtomicInteger registeredQueryHandlersCount = new AtomicInteger();
            AtomicInteger registeredCommandHandlersCount = new AtomicInteger();
            queryHandlers.forEach(e -> {
                ParameterizedType genericSuperclass = (ParameterizedType) ClassUtils.getUserClass(e).getGenericInterfaces()[0];
                Type queryType = genericSuperclass.getActualTypeArguments()[1];
                Class queryClass = (Class)queryType;
                log.debug("Registered queryHandler {} for Query {}",e.getClass().getName(), queryClass);
                queryHandlerFactory.addQueryHandler(e, queryClass);
                if (queryClass.getAnnotationsByType(RestAction.class).length > 0) {
                    val url = internalModuleRemoteQueryHandlerFactory.addQuery(queryClass);
                    log.debug("Registered remote query {} available at url {}", queryClass.getSimpleName(), url);
                }
                if (queryClass.getAnnotationsByType(ExternalRestAction.class).length > 0) {
                    val url = externalModuleRemoteQueryHandlerFactory.addQuery(queryClass);
                    log.debug("Registered external module remote query {} available at url {}", queryClass.getSimpleName(), url);
                }
                registeredQueryHandlersCount.getAndIncrement();
            });
            commandHandlers.forEach(e -> {
                ParameterizedType genericSuperclass = (ParameterizedType) ClassUtils.getUserClass(e).getGenericInterfaces()[0];
                Type commandType = genericSuperclass.getActualTypeArguments()[1];
                Class commandClass = (Class)commandType;
                log.debug("Registered commandHandler {} for Command {}", e.getClass().getName(), commandClass);
                commandHandlerFactory.addCommandHandler(e, commandClass);
                if (commandClass.getAnnotationsByType(RestAction.class).length > 0) {
                    val url = internalModuleRemoteCommandHandlerFactory.addCommand(commandClass);
                    log.debug("Registered remote command {} available at url {}", commandClass.getSimpleName(), url);
                }
                if (commandClass.getAnnotationsByType(ExternalRestAction.class).length > 0) {
                    val url = externalModuleRemoteCommandHandlerFactory.addCommand(commandClass);
                    log.debug("Registered external module remote command {} available at url {}", commandClass.getSimpleName(), url);
                }
                registeredCommandHandlersCount.getAndIncrement();
            });
            log.debug("Registered {} queryHandlers", registeredQueryHandlersCount);
            log.debug("Registered {} commandHandlers", registeredCommandHandlersCount);
        }
    }
}
