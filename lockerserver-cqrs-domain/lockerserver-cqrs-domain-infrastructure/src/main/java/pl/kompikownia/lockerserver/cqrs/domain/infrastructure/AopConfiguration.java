package pl.kompikownia.lockerserver.cqrs.domain.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.cqrs.domain.application.aop.HandlerExecutionAop;

@Configuration
public class AopConfiguration {

    @Bean
    public HandlerExecutionAop handlerExecutionAop() {
        return new HandlerExecutionAop();
    }
}
