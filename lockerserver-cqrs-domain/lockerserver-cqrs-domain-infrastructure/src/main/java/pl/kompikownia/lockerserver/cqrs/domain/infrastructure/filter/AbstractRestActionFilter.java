package pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import pl.kompikownia.lockerserver.common.utils.domain.exception.AuthorizationException;
import pl.kompikownia.lockerserver.common.utils.domain.exception.BusinessException;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.CommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.QueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.RemoteCommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.RemoteQueryHandlerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public abstract class AbstractRestActionFilter implements Filter {

    private final String RESPONSE_CONTENT_TYPE = "application/json; charset=UTF-8";
    private final String CHARACTER_ENCODING = "UTF-8";

    private final RemoteQueryHandlerFactory remoteQueryFactory;
    private final RemoteCommandHandlerFactory remoteCommandFactory;
    private final QueryHandlerFactory queryHandlerFactory;
    private final CommandHandlerFactory commandHandlerFactory;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        val httpRequest = (HttpServletRequest) servletRequest;
        val httpResponse = (HttpServletResponse) servletResponse;
        httpResponse.setContentType(RESPONSE_CONTENT_TYPE);
        httpRequest.setCharacterEncoding(CHARACTER_ENCODING);
        val url = httpRequest.getRequestURI();
        val queryOptional = remoteQueryFactory.getQueryByUrl(url);
        val commandOptional = remoteCommandFactory.getCommandByUrl(url);
        if (httpRequest.getMethod().equals(HttpMethod.POST.name())) {
            if(queryOptional.isPresent()) {
                setContext(httpRequest);
                processQuery(queryOptional.get(), httpRequest, httpResponse);
                clearContext();
                return;
            }
            if(commandOptional.isPresent()) {
                setContext(httpRequest);
                processCommand(commandOptional.get(), httpRequest, httpResponse);
                clearContext();
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void processQuery(Class<Query<?>> query, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        val objectMapper = new ObjectMapper();
        QueryHandler handler = (QueryHandler<?,? extends Query<?>>) queryHandlerFactory.getQueryHandlerByQueryClass(query);
        val queryObject = createActionObject(query, httpRequest.getReader().lines().collect(Collectors.joining()));
        try {
            val queryResult = handler.handle((Query) queryObject);
            val out = httpResponse.getWriter();
            out.print(objectMapper.writeValueAsString(queryResult));
        }
        catch (AuthorizationException ex) {
            httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        }

    }

    private void processCommand(Class<Command<?>> command, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        val objectMapper = new ObjectMapper();
        CommandHandler handler = (CommandHandler<?, ? extends Command<?>>) commandHandlerFactory.getCommandHandlerByCommandClass(command);
        val commandObject = createActionObject(command, httpRequest.getReader().lines().collect(Collectors.joining()));
        try {
            val commandResult = handler.handle((Command)commandObject);
            val out = httpResponse.getWriter();
            out.print(objectMapper.writeValueAsString(commandResult));
        }
        catch (AuthorizationException ex) {
            httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        }

    }

    private Object createActionObject(Class<?> objectClass, String body) throws IOException {
        try {
            val objectMapper = new ObjectMapper();
            return objectMapper.readValue(body, objectClass);

        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    public abstract void setContext(HttpServletRequest request);
    public abstract void clearContext();
}
