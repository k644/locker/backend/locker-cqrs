package pl.kompikownia.lockerserver.cqrs.domain.infrastructure.adapter;

import com.fasterxml.jackson.databind.util.ClassUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.util.ClassUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.kompikownia.lockerserver.common.utils.domain.context.Context;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.UserContext;
import pl.kompikownia.lockerserver.cqrs.domain.api.Action;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.annotation.RestAction;
import pl.kompikownia.lockerserver.cqrs.domain.api.exception.ServiceExecutorException;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.Protocol;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceUrlProvider;
import pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter.ProcessRestActionFilter;

import java.lang.reflect.ParameterizedType;

@Slf4j
@RequiredArgsConstructor
public class ServiceExecutorImpl implements ServiceExecutor {

    private static final String COMMAND_CLASS_NAME = "pl.kompikownia.lockerserver.cqrs.domain.api.Command";
    private static final String QUERY_CLASS_NAME = "pl.kompikownia.lockerserver.cqrs.domain.api.Query";

    private final ServiceUrlProvider serviceUrlProvider;
    private final ContextHolder<? extends UserContext> contextHolder;
    private final RestTemplate serviceRestTemplate;
    private final RestTemplate externalRestTemplate;

    @Override
    public <T, Q extends Query<T>> T executeQuery(Protocol protocol, Q query, String hostname, String port) {
        val genericSuperClass = (ParameterizedType) ClassUtils.getUserClass(query).getGenericInterfaces()[0];
        val responseClass = (Class<T>)genericSuperClass.getActualTypeArguments()[0];
        val url = String.format("%s%s:%s/%s",protocol.getProtocolUrl(), hostname, port, query.getClass().getSimpleName());
        log.debug("Start query {} ({})", query.getClass().getSimpleName(), url);
        long start = System.currentTimeMillis();
        try {
            val result = externalRestTemplate.getForObject(getUriQueryParams(query, url), responseClass);
            Long end = System.currentTimeMillis() - start;
            log.debug("Execution of query {} finished in time {} ms with result {}", query.getClass().getSimpleName(), end, result);
            return result;
        }
        catch(HttpClientErrorException ex) {
            Long end = System.currentTimeMillis() - start;
            log.error("Execution of query {} finished with errors in time {} ms", query.getClass().getSimpleName(), end);
            throw new ServiceExecutorException(query.getClass().getSimpleName(), url, ex.getStatusCode().value());
        }
    }

    @Override
    public <T, C extends Command<T>> T executeCommand(Protocol protocol, C command, String hostname, String port) {
        val genericSuperClass = (ParameterizedType) ClassUtils.getUserClass(command).getGenericInterfaces()[0];
        val responseClass = (Class<T>)genericSuperClass.getActualTypeArguments()[0];
        val url = String.format("%s%s:%s/%s",protocol.getProtocolUrl(), hostname, port, command.getClass().getSimpleName());
        log.debug("Start command {} ({})", command.getClass().getSimpleName(), url);
        long start = System.currentTimeMillis();
        try {
            val result = externalRestTemplate.postForObject(url, command, responseClass);
            Long end = System.currentTimeMillis() - start;
            log.debug("Execution of command {} finished in time {} ms with result {}", command.getClass().getSimpleName(), end, result);
            return result;
        }
        catch(HttpClientErrorException ex) {
            Long end = System.currentTimeMillis() - start;
            log.error("Execution of command {} finished with errors in time {} ms", command.getClass().getSimpleName(), end);
            throw new ServiceExecutorException(command.getClass().getSimpleName(), url, ex.getStatusCode().value());
        }
    }

    @Override
    public <T, C extends Action<T>> T executeAction(C action) {
        val genericSuperClass = (ParameterizedType) ClassUtils.getUserClass(action).getGenericInterfaces()[0];
        if (ClassUtils.getUserClass(action).getGenericSuperclass().getTypeName().equals("java.lang.Object")) {
            return executeSimpleAction(action);
        }
        val responseClassParameterizedType = (ParameterizedType)genericSuperClass.getActualTypeArguments()[0];
        val restActionAnnotation = action.getClass().getAnnotationsByType(RestAction.class);
        if (restActionAnnotation.length > 0) {
            val module = restActionAnnotation[0].value();
            val basicUrl = serviceUrlProvider.getConnectionURLForService(module);
            if(basicUrl.isEmpty()) {
                log.error("Cannot find remote url for service {}", action.getClass().getSimpleName());
                throw new ServiceExecutorException(action.getClass().getSimpleName(), "", 404);
            }
            val fullUrl = String.format("%s/%s/%s", basicUrl.get(), module.name().toLowerCase(), action.getClass().getSimpleName().toLowerCase());
            long start = System.currentTimeMillis();
            log.debug("Start action {}", action.getClass().getSimpleName());
            try {
                val httpHeaders = new HttpHeaders();
                httpHeaders.set(ProcessRestActionFilter.USER_ID_HEADER, contextHolder.getContext().getUserId());
                httpHeaders.set(ProcessRestActionFilter.LANGUAGE_HEADER, contextHolder.getContext().getLanguage().name());
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<C> entity = new HttpEntity<C>(action, httpHeaders);
                val result = serviceRestTemplate.exchange(fullUrl, HttpMethod.POST, entity, ParameterizedTypeReference.forType(responseClassParameterizedType))
                        .getBody();
                Long end = System.currentTimeMillis() - start;
                log.debug("Execution of action {} finished in time {} ms with result {}", action.getClass().getSimpleName(), end, result);
                return (T)result;
            }
            catch(HttpClientErrorException ex) {
                Long end = System.currentTimeMillis() - start;
                log.error("Execution of action {} finished with errors in time {} ms", action.getClass().getSimpleName(), end);
                throw new ServiceExecutorException(action.getClass().getSimpleName(), fullUrl, ex.getStatusCode().value());
            }
        }
        log.error("Service {} is not remote microservice", action.getClass().getSimpleName());
        throw new ServiceExecutorException(action.getClass().getSimpleName(), "", HttpStatus.NOT_FOUND.value());
    }

    private <T, C extends Action<T>> T executeSimpleAction(C action) {
        val genericSuperClass = (ParameterizedType) ClassUtils.getUserClass(action).getGenericInterfaces()[0];
        val responseClass = (Class<T>)genericSuperClass.getActualTypeArguments()[0];
        val restActionAnnotation = action.getClass().getAnnotationsByType(RestAction.class);
        if (restActionAnnotation.length > 0) {
            val module = restActionAnnotation[0].value();
            val basicUrl = serviceUrlProvider.getConnectionURLForService(module);
            if(basicUrl.isEmpty()) {
                log.error("Cannot find remote url for service {}", action.getClass().getSimpleName());
                throw new ServiceExecutorException(action.getClass().getSimpleName(), "", HttpStatus.NOT_FOUND.value());
            }
            val fullUrl = String.format("%s/api/internal/%s/%s", basicUrl.get(), module.name().toLowerCase(), action.getClass().getSimpleName().toLowerCase());
            long start = System.currentTimeMillis();
            log.debug("Start action {}", action.getClass().getSimpleName());
            try {
                val httpHeaders = new HttpHeaders();
                if(contextHolder.getContext().getUserId() != null) {
                    httpHeaders.set(ProcessRestActionFilter.USER_ID_HEADER, contextHolder.getContext().getUserId());
                }
                if(contextHolder.getContext().getLanguage() != null) {
                    httpHeaders.set(ProcessRestActionFilter.LANGUAGE_HEADER, contextHolder.getContext().getLanguage().name());
                }
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<C> entity = new HttpEntity<C>(action, httpHeaders);
                val result = serviceRestTemplate.exchange(fullUrl, HttpMethod.POST, entity, responseClass).getBody();
                Long end = System.currentTimeMillis() - start;
                log.debug("Execution of action {} finished in time {} ms with result {}", action.getClass().getSimpleName(), end, result);
                return result;
            }
            catch(HttpClientErrorException ex) {
                Long end = System.currentTimeMillis() - start;
                log.error("Execution of action {} finished with errors in time {} ms", action.getClass().getSimpleName(), end);
                throw new ServiceExecutorException(action.getClass().getSimpleName(), fullUrl, ex.getStatusCode().value());
            }
        }
        log.error("Service {} is not remote microservice", action.getClass().getSimpleName());
        throw new ServiceExecutorException(action.getClass().getSimpleName(), "", HttpStatus.NOT_FOUND.value());
    }


    private<T, Q extends Query<T>> String getUriQueryParams(Q query, String httpUrl) {
        val uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(httpUrl);
        val fields = query.getClass().getDeclaredFields();
        try {
            for (val field: fields) {
                field.setAccessible(true);
                uriComponentsBuilder.queryParam(field.getName(), field.get(query));
                field.setAccessible(false);
            }
        }
        catch(IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return uriComponentsBuilder.toUriString();
    }
}
