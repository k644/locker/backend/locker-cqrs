package pl.kompikownia.lockerserver.cqrs.domain.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.processor.GetDataProcessor;

@Configuration
public class ApiGatewayHelperConfiguration {

    @Bean
    public GetDataProcessor getDataProcessor(final QueryExecutor queryExecutor) {
        return new GetDataProcessor(queryExecutor);
    }
}
