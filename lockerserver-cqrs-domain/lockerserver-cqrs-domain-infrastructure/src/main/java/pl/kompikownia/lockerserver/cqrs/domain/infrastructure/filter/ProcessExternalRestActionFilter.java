package pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.annotation.Order;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.external.module.ExternalModuleContext;
import pl.kompikownia.lockerserver.common.utils.domain.exception.ModuleNotFoundException;
import pl.kompikownia.lockerserver.common.utils.domain.locker.module.LockerModuleContextDataProvider;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@Slf4j
@Order(3)
public class ProcessExternalRestActionFilter extends AbstractRestActionFilter {
    private static final String MODULE_ID_HEADER = "MODULE_ID";

    private final ContextHolder<ExternalModuleContext> externalModuleContextHolder;
    private final ContextFactory<ExternalModuleContext> externalModuleContextFactory;
    private final LockerModuleContextDataProvider lockerModuleContextDataProvider;

    public ProcessExternalRestActionFilter(ExternalModuleRemoteQueryHandlerFactory remoteQueryFactory,
                                           ExternalModuleRemoteCommandHandlerFactory remoteCommandFactory,
                                           QueryHandlerFactory queryHandlerFactory,
                                           CommandHandlerFactory commandHandlerFactory,
                                           ContextHolder<ExternalModuleContext> externalModuleContextHolder,
                                           ContextFactory<ExternalModuleContext> externalModuleContextFactory,
                                           LockerModuleContextDataProvider lockerModuleContextDataProvider) {
        super(remoteQueryFactory, remoteCommandFactory, queryHandlerFactory, commandHandlerFactory);
        this.externalModuleContextFactory = externalModuleContextFactory;
        this.externalModuleContextHolder = externalModuleContextHolder;
        this.lockerModuleContextDataProvider = lockerModuleContextDataProvider;
    }

    @Override
    public void setContext(HttpServletRequest request) {
        val moduleId = request.getHeader(MODULE_ID_HEADER);
        val module = lockerModuleContextDataProvider.findLockerModuleBySerialNumber(moduleId);
        if(module.isEmpty()) throw new ModuleNotFoundException(moduleId);
        val context = externalModuleContextFactory.buildEmptyContext();
        context.setAssignedLockerId(module.get().getOwnerLockerId());
        context.setSerialNumber(module.get().getSerialNumber());
        externalModuleContextHolder.setContext(context);
    }

    @Override
    public void clearContext() {
        externalModuleContextHolder.clearContext();
    }
}
