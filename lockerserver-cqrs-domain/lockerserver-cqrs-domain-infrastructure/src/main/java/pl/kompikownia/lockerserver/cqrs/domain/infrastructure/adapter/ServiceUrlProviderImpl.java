package pl.kompikownia.lockerserver.cqrs.domain.infrastructure.adapter;

import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import pl.kompikownia.lockerserver.cqrs.domain.api.SystemModuleType;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceUrlProvider;

import java.util.Optional;

@RequiredArgsConstructor
public class ServiceUrlProviderImpl implements ServiceUrlProvider {

    private final Environment environment;

    @Override
    public Optional<String> getConnectionURLForService(SystemModuleType service) {
        return Optional.ofNullable(environment.getProperty("pl.kompikownia.lockerserver."+service.name().toLowerCase()+".url"));
    }
}
