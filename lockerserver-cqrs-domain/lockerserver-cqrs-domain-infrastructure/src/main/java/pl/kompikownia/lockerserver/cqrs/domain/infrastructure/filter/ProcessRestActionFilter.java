package pl.kompikownia.lockerserver.cqrs.domain.infrastructure.filter;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.annotation.Order;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;
import pl.kompikownia.lockerserver.common.utils.domain.types.Language;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.CommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.InternalModuleRemoteCommandHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.InternalModuleRemoteQueryHandlerFactory;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.QueryHandlerFactory;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Order(2)
public class ProcessRestActionFilter extends AbstractRestActionFilter {
    public static final String USER_ID_HEADER = "USER_ID";
    public static final String LANGUAGE_HEADER = "BUSINESS_LANGUAGE";
    private final ContextHolder<ModuleContext> moduleContextHolder;
    private final ContextFactory<ModuleContext> moduleContextFactory;

    public ProcessRestActionFilter(InternalModuleRemoteQueryHandlerFactory remoteQueryFactory,
                                   InternalModuleRemoteCommandHandlerFactory remoteCommandFactory,
                                   QueryHandlerFactory queryHandlerFactory,
                                   CommandHandlerFactory commandHandlerFactory,
                                   ContextHolder<ModuleContext> moduleContextHolder,
                                   ContextFactory<ModuleContext> moduleContextFactory) {
        super(remoteQueryFactory, remoteCommandFactory, queryHandlerFactory, commandHandlerFactory);
        this.moduleContextHolder = moduleContextHolder;
        this.moduleContextFactory = moduleContextFactory;
    }

    @Override
    public void setContext(HttpServletRequest request) {
        val userId = request.getHeader(USER_ID_HEADER);
        val language = request.getHeader(LANGUAGE_HEADER);
        val context = moduleContextFactory.buildEmptyContext();
        context.setUserId(userId);
        context.setLanguage(language != null ? Language.valueOf(language) : null);
        moduleContextHolder.setContext(context);
        log.debug("Module context was created ({})", context.toString());
    }

    @Override
    public void clearContext() {
        moduleContextHolder.clearContext();
        log.debug("Module context was erased");
    }
}
