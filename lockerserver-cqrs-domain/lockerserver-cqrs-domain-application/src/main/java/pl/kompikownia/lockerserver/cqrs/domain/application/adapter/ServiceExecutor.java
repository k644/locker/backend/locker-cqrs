package pl.kompikownia.lockerserver.cqrs.domain.application.adapter;

import pl.kompikownia.lockerserver.cqrs.domain.api.Action;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.Protocol;

public interface ServiceExecutor {
    <T,Q extends Query<T>> T executeQuery(Protocol protocol, Q query, String hostname, String port);
    <T,C extends Command<T>> T executeCommand(Protocol protocol,C command, String hostname, String port);
    <T, C extends Action<T>> T executeAction(C action);
}
