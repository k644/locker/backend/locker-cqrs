package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

import java.util.Optional;

public interface RemoteCommandHandlerFactory {
    String addCommand(Class<Command<?>> command);
    Optional<Class<Command<?>>> getCommandByUrl(String url);
}
