package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;

import java.util.HashMap;
import java.util.Map;

public class CommandHandlerFactory {

    private Map<Class<Command<?>>, CommandHandler<?, ? extends Command<?>>> commandHandlerMap = new HashMap<>();

    public Object getCommandHandlerForCommand(Object command) {
        return commandHandlerMap.get(command.getClass());
    }

    public void addCommandHandler(CommandHandler<?, ? extends Command<?>> commandHandler, Class<Command<?>> command) {
        commandHandlerMap.put(command, commandHandler);
    }

    public Object getCommandHandlerByCommandClass(Class<?> command) {
        return commandHandlerMap.get(command);
    }
}
