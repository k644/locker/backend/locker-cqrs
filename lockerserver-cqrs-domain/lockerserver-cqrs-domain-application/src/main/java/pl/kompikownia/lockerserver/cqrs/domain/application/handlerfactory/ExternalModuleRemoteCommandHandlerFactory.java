package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import lombok.val;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.annotation.ExternalRestAction;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ExternalModuleRemoteCommandHandlerFactory implements RemoteCommandHandlerFactory{

    private Map<String, Class<Command<?>>> remoteCommandMap = new HashMap<>();

    @Override
    public String addCommand(Class<Command<?>> command) {
        val module = command.getAnnotationsByType(ExternalRestAction.class)[0].value().name();
        val url = String.format("/module/%s/%s", module.toLowerCase(), command.getSimpleName().toLowerCase());
        remoteCommandMap.put(url, command);
        return url;
    }

    @Override
    public Optional<Class<Command<?>>> getCommandByUrl(String url) {
        return Optional.ofNullable(remoteCommandMap.get(url));
    }
}
