package pl.kompikownia.lockerserver.cqrs.domain.application.executor;

import lombok.RequiredArgsConstructor;
import pl.kompikownia.lockerserver.cqrs.domain.api.Command;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.CommandExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.Protocol;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.CommandHandlerFactory;

@RequiredArgsConstructor
public class CommandExecutorImpl implements CommandExecutor {

    private final CommandHandlerFactory commandHandlerFactory;
    private final ServiceExecutor serviceExecutor;
    private final Protocol protocol;

    @Override
    public <T, Q extends Command<T>> T execute(Q q) {
        CommandHandler<T, Q> commandHandler = (CommandHandler<T, Q>) commandHandlerFactory.getCommandHandlerForCommand(q);
        if(commandHandler != null) {
            return commandHandler.handle(q);
        }
        else {
            return serviceExecutor.executeAction(q);
        }
    }

    @Override
    public <T, Q extends Command<T>> T execute(Q q, String hostname, String port) {
        return serviceExecutor.executeCommand(protocol, q, hostname, port);
    }
}
