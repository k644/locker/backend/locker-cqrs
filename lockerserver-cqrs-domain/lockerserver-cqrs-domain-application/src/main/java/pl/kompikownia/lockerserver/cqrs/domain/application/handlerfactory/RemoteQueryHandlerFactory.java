package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

import java.util.Optional;

public interface RemoteQueryHandlerFactory {
    String addQuery(Class<Query<?>> query);
    Optional<Class<Query<?>>> getQueryByUrl(String url);
}
