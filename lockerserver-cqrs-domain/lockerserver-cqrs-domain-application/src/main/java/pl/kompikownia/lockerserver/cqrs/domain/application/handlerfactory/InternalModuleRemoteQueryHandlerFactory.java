package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import lombok.val;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.annotation.RestAction;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InternalModuleRemoteQueryHandlerFactory implements RemoteQueryHandlerFactory {

    private Map<String, Class<Query<?>>> remoteQueryMap = new HashMap<>();

    @Override
    public String addQuery(Class<Query<?>> query) {
        val module = query.getAnnotationsByType(RestAction.class)[0].value().name();
        val url = String.format("/api/internal/%s/%s", module.toLowerCase(), query.getSimpleName().toLowerCase());
        remoteQueryMap.put(url, query);
        return url;
    }

    @Override
    public Optional<Class<Query<?>>> getQueryByUrl(String url) {
        return Optional.ofNullable(remoteQueryMap.get(url));
    }
}
