package pl.kompikownia.lockerserver.cqrs.domain.application.executor;

import lombok.RequiredArgsConstructor;
import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.Protocol;
import pl.kompikownia.lockerserver.cqrs.domain.application.adapter.ServiceExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory.QueryHandlerFactory;

@RequiredArgsConstructor
public class QueryExecutorImpl implements QueryExecutor {

    private final QueryHandlerFactory queryHandlerFactory;
    private final ServiceExecutor serviceExecutor;
    private final Protocol protocol;

    @Override
    public <T, Q extends Query<T>> T execute(Q query) {
        QueryHandler<T, Q> queryHandler = (QueryHandler<T, Q>) queryHandlerFactory.getQueryHandlerByQuery(query);
        if (queryHandler != null) {
            return queryHandler.handle(query);
        }
        else {
            return serviceExecutor.executeAction(query);
        }
    }

    @Override
    public <T, Q extends Query<T>> T execute(Q q, String hostname, String port) {
        return serviceExecutor.executeQuery(protocol, q, hostname, port);
    }
}
