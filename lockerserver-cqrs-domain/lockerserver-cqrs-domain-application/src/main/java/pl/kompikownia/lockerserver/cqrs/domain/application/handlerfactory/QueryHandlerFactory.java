package pl.kompikownia.lockerserver.cqrs.domain.application.handlerfactory;

import pl.kompikownia.lockerserver.cqrs.domain.api.Query;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;

import java.util.HashMap;
import java.util.Map;

public class QueryHandlerFactory {

    private Map<Class<Query<?>>, QueryHandler<?, ? extends Query<?>>> queryHandlerMap = new HashMap<>();

    public Object getQueryHandlerByQuery(Object query) {
        return queryHandlerMap.get(query.getClass());
    }

    public Object getQueryHandlerByQueryClass(Class<?> query) {
        return queryHandlerMap.get(query);
    }

    public void addQueryHandler(QueryHandler<?, ? extends Query<?>> queryHandler, Class<Query<?>> query) {
        queryHandlerMap.put(query, queryHandler);
    }
}
