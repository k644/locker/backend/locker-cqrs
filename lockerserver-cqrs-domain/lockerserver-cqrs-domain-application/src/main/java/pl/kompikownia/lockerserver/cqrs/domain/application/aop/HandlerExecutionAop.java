package pl.kompikownia.lockerserver.cqrs.domain.application.aop;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
@Slf4j
public class HandlerExecutionAop {

    @Pointcut("within (pl.kompikownia.lockerserver.cqrs.domain.api.handler.CommandHandler+)")
    private void allCommandHandlersPointcut() { }

    @Pointcut("within (pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler+)")
    private void allQueryHandlersPointcut() { }

    @Pointcut("execution(* handle(..))")
    private void executionHandleMethodPointcut() { }

    @Around("(allCommandHandlersPointcut() || allQueryHandlersPointcut()) && executionHandleMethodPointcut()")
    public Object method(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        val methodName = proceedingJoinPoint.getSignature().getDeclaringTypeName();
        val arguments = proceedingJoinPoint.getArgs()[0].toString();
        log.debug("Execution handler started for {} with arguments {}", methodName, arguments);
        long start = System.currentTimeMillis();
        val result = proceedingJoinPoint.proceed();
        Long end = System.currentTimeMillis() - start;
        log.debug("Execution handler ended for {} with result {} in time {} ms", methodName, result, end);
        return result;
    }
}
