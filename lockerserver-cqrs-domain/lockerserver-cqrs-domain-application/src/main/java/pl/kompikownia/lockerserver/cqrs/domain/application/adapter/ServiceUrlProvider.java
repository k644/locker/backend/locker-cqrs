package pl.kompikownia.lockerserver.cqrs.domain.application.adapter;

import pl.kompikownia.lockerserver.cqrs.domain.api.SystemModuleType;

import java.util.Optional;

public interface ServiceUrlProvider {
    Optional<String> getConnectionURLForService(SystemModuleType service);
}
