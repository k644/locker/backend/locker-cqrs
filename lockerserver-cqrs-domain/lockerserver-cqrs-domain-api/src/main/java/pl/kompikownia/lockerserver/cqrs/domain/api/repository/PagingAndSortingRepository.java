package pl.kompikownia.lockerserver.cqrs.domain.api.repository;

import pl.kompikownia.lockerserver.common.utils.domain.types.query.SearchResult;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.SortField;

import java.util.List;
import java.util.Map;

public interface PagingAndSortingRepository<T> {
    SearchResult<List<T>> findBySortingAndFilteringParameters(
            String liveSearchQuery,
            Map<String, String> filterFields,
            SortField sortField,
            Integer page,
            Integer elementsOnPage
    );
}
