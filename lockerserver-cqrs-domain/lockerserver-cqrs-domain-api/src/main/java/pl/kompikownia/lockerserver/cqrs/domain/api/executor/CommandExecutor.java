package pl.kompikownia.lockerserver.cqrs.domain.api.executor;

import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

public interface CommandExecutor {
    <T, Q extends Command<T>> T execute(Q q);
    <T, Q extends Command<T>> T execute(Q q, String hostname, String port);
}
