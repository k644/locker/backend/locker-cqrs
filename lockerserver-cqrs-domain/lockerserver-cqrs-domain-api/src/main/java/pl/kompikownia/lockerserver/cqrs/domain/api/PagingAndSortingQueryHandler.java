package pl.kompikownia.lockerserver.cqrs.domain.api;

import lombok.RequiredArgsConstructor;
import lombok.val;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.PagingAndSortingResult;
import pl.kompikownia.lockerserver.cqrs.domain.api.handler.QueryHandler;
import pl.kompikownia.lockerserver.cqrs.domain.api.repository.PagingAndSortingRepository;

@RequiredArgsConstructor
public abstract class PagingAndSortingQueryHandler <View, Query extends PagingAndSortingQuery<View>>  implements QueryHandler<PagingAndSortingResult<View>, Query> {

    private final PagingAndSortingRepository<View> repository;

    @Override
    public PagingAndSortingResult<View> handle(Query query) {
        val filteringResult = repository.findBySortingAndFilteringParameters(
                query.getQuery(),
                query.getFilterFields(),
                query.getSortField(),
                query.getPageNumber(),
                query.getElementsCount()
        );
        return PagingAndSortingResult.<View>builder()
                .actualPage(query.getPageNumber())
                .allElementsCount(filteringResult.getElementsCount())
                .elementsCount((long) filteringResult.getElement().size())
                .element(filteringResult.getElement())
                .build();
    }
}
