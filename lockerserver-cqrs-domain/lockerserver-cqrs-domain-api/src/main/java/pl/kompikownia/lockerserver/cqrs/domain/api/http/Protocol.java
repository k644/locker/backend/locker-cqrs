package pl.kompikownia.lockerserver.cqrs.domain.api.http;

public enum Protocol {
    HTTP("http", "http://"),
    HTTPS("https", "https://");
    private String protocol;
    private String protocolUrl;

    public String getProtocol() {
        return protocol;
    }

    public String getProtocolUrl() {
        return protocolUrl;
    }

    Protocol(String protocol, String protocolUrl) {
        this.protocol = protocol;
        this.protocolUrl = protocolUrl;
    }

}
