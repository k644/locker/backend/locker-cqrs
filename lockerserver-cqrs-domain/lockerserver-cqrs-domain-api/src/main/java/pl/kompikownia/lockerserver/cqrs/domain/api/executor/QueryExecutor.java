package pl.kompikownia.lockerserver.cqrs.domain.api.executor;

import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

public interface QueryExecutor {
    <T, Q extends Query<T>> T execute(Q q);
    <T, Q extends Query<T>> T execute(Q q, String hostname, String port);
}
