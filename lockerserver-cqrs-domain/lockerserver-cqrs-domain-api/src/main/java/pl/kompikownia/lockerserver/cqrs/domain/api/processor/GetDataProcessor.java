package pl.kompikownia.lockerserver.cqrs.domain.api.processor;

import lombok.RequiredArgsConstructor;
import lombok.val;
import pl.kompikownia.lockerserver.common.utils.domain.converter.Converter;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.SortDirection;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.SortField;
import pl.kompikownia.lockerserver.cqrs.domain.api.PagingAndSortingQuery;
import pl.kompikownia.lockerserver.cqrs.domain.api.executor.QueryExecutor;
import pl.kompikownia.lockerserver.cqrs.domain.api.http.GetResponse;

import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class GetDataProcessor{

    private final QueryExecutor queryExecutor;

    private Map<String, SortDirection> sortDirectionMap = Map.of(
            "ASC", SortDirection.ASC,
            "asc", SortDirection.ASC,
            "DESC", SortDirection.DESC,
            "desc", SortDirection.DESC
    );

    public <View, T extends PagingAndSortingQuery<View>,ResponseObject> GetResponse<ResponseObject> processRequest(
            String q,
            Supplier<Map<String, String>> filterFieldMap,
            Integer limit,
            Integer page,
            String sort,
            String sortDirection,
            T queryBuilder,
            Converter<ResponseObject, View> toResponseObjectConverter
    ) {
        queryBuilder.setQuery(q);
        queryBuilder.setElementsCount(limit);
        queryBuilder.setPageNumber(page);
        queryBuilder.setFilterFields(filterFieldMap.get());
        if (sort != null && sortDirection != null) {
            queryBuilder.setSortField(SortField.of(sort, sortDirectionMap.getOrDefault(sortDirection, SortDirection.ASC)));
        }
        val result = queryExecutor.execute(queryBuilder);
        return GetResponse.<ResponseObject>builder()
                .actualPage(result.getActualPage())
                .elementsCount(result.getAllElementsCount())
                .elements(result.getElement().stream()
                        .map(toResponseObjectConverter::convert)
                        .collect(Collectors.toList()))
                .build();
    }
}
