package pl.kompikownia.lockerserver.cqrs.domain.api.handler;

import pl.kompikownia.lockerserver.cqrs.domain.api.Query;

public interface QueryHandler<Result, Q extends Query<Result>> {
    Result handle(Q query);
}
