package pl.kompikownia.lockerserver.cqrs.domain.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.PagingAndSortingResult;
import pl.kompikownia.lockerserver.common.utils.domain.types.query.SortField;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class PagingAndSortingQuery<T> implements Query<PagingAndSortingResult<T>> {
    private String query;
    private Map<String, String> filterFields = new HashMap<>();
    private SortField sortField;
    private Integer pageNumber;
    private Integer elementsCount;
}
