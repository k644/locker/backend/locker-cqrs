package pl.kompikownia.lockerserver.cqrs.domain.api.http;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class GetResponse<T> {
    private Integer actualPage;
    private Long elementsCount;
    private List<T> elements;
}
