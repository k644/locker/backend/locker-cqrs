package pl.kompikownia.lockerserver.cqrs.domain.api.handler;

import pl.kompikownia.lockerserver.cqrs.domain.api.Command;

public interface CommandHandler<Result, C extends Command<Result>> {
    Result handle(C c);
}
