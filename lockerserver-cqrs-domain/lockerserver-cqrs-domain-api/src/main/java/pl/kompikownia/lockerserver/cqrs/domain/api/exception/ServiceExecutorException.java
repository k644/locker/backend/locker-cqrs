package pl.kompikownia.lockerserver.cqrs.domain.api.exception;

        import lombok.Getter;

@Getter
public class ServiceExecutorException extends RuntimeException {
    private int httpStatus;
    public ServiceExecutorException(String query, String url, int resultCode) {
        super(String.format("There was problem during executing service %s (%s). Ended with code %s", query, url, resultCode));
        this.httpStatus = resultCode;
    }
}
