package pl.kompikownia.lockerserver.cqrs.domain.api.annotation;

import pl.kompikownia.lockerserver.cqrs.domain.api.SystemModuleType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ExternalRestAction {
    SystemModuleType value();
}
