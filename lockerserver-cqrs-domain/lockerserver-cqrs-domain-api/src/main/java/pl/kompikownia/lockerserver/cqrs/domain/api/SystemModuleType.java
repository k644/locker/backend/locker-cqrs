package pl.kompikownia.lockerserver.cqrs.domain.api;

public enum SystemModuleType {
    LOCKER, LOGIN, AUTHORIZATION_DEVICES,EVENT
}
